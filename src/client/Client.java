package client;
import view.AdminView;
import view.ClientView;
import view.LoginView;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class Client {
    //modify server_address as needed : d524lab_pc48
    private static final String SERVER_ADDRESS = "jopnigga";
    private static final int SERVER_PORT = 8081;

    public static void main(String[] args) {
        try {
            Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
            System.out.println("Connected to server.");

            SwingUtilities.invokeLater(new Runnable() {
                @Override

                public void run() {
                    new LoginView(socket).setVisible(true);
                }
            });
            String response = "";
            do {
                ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());
                response = reader.readUTF();
                System.out.println(response);
                switch (response) {
                    case "admin":
                        try {
                            Socket socket1 = new Socket(SERVER_ADDRESS,SERVER_PORT);
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                  AdminView adminView =  new AdminView(socket);
                                    adminView.setVisible(true);
                                    adminView.loadUsersFromXML(socket1);
                                    adminView.operateUser(socket1);
                                    adminView.setUpAdd(socket);
                                    adminView.search(socket1);
                                    adminView.setUpDelete(socket);
                                }
                            });
                        } catch (Exception adminError) {
                            adminError.printStackTrace();
                        }
                        break;
                    case "user":
                        try {
                            Socket socket2 = new Socket(SERVER_ADDRESS,SERVER_PORT);
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    ClientView clientView = new ClientView(socket);
                                    clientView.setVisible(true);
                                    clientView.loadUsersFromXML(socket2);
                                    clientView.setUpAdd(socket2);
                                    clientView.setUpDelete(socket);
                                    clientView.search(socket2);
                                }
                            });
                        } catch (Exception userError) {
                            userError.printStackTrace();
                        }
                        break;

                    case "again":
                        try {
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    LoginView newLogin = new LoginView(socket);
                                    JOptionPane.showMessageDialog(newLogin, "login unsuccessful, please try again");
                                    newLogin.setVisible(true);
                                }
                            });
                        } catch (Exception tryAgainError) {
                            tryAgainError.printStackTrace();
                        }
                        break;
                    case "signup":
                        try {
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    LoginView afterLogin = new LoginView(socket);
                                    JOptionPane.showMessageDialog(afterLogin,"signup successful");
                                    afterLogin.setVisible(true);
                                }
                            });
                        }catch (Exception signUpError){
                            signUpError.printStackTrace();
                        }
                }
            }
            while (socket.isConnected());
        } catch (IOException e) {
            System.out.println("client disconnected: " + e.getMessage());
        }
    }
}
