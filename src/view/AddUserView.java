package view;

import javax.swing.*;
import java.awt.*;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class AddUserView extends JFrame {
    private JTextField emailField;
    private JTextField nameField;
    private JTextField passwordField;
    private JTextField typeField;
    private int result;

    public AddUserView() {
        setTitle("Add user");
        setSize(300, 200);

        JPanel myPanel = new JPanel();
        myPanel.setLayout(new GridLayout(5, 2));
        myPanel.add(new JLabel("email: "));
        emailField = new JTextField(10);
        myPanel.add(emailField);
        myPanel.add(new JLabel("username: "));
        nameField = new JTextField(10);
        myPanel.add(nameField);
        myPanel.add(new JLabel("password: "));
        passwordField = new JTextField(10);
        myPanel.add(passwordField);
        myPanel.add(new JLabel("type:"));
        typeField = new JTextField(10);
        myPanel.add(typeField);
        result = JOptionPane.showConfirmDialog(null,myPanel,
                "add user",JOptionPane.OK_CANCEL_OPTION);
    }

    public ArrayList<String> sendNewUser() {
        ArrayList<String> userAdd = new ArrayList<>();
        if (result == JOptionPane.OK_OPTION) {
            userAdd.add(emailField.getText());
            userAdd.add(nameField.getText());
            userAdd.add(passwordField.getText());
            userAdd.add(typeField.getText());
            JOptionPane.showMessageDialog(null, "Added User Successfully.");
            dispose();
        } else {
            dispose();
        }
        return userAdd;
    }
}
