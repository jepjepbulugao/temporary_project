package view;

import javax.swing.*;

import model.ClientModel;

import java.awt.*;
import java.util.ArrayList;

public class CreateEventView extends JFrame {
    private JTextField eventField;
    private JTextField dateField;
    private JTextField timeField;
    private JTextField venueField;
    private JTextField descriptionField;
    private boolean eventCreatedSuccessfully;

    public CreateEventView() {
        setTitle("Create Event");
        setSize(300, 200);

        JPanel myPanel = new JPanel();
        myPanel.setLayout(new GridLayout(5, 2));
        myPanel.add(new JLabel("Name:"));
        eventField = new JTextField(10);
        myPanel.add(eventField);
        myPanel.add(new JLabel("Date:"));
        dateField = new JTextField(10);
        myPanel.add(dateField);
        myPanel.add(new JLabel("Time:"));
        timeField = new JTextField(10);
        myPanel.add(timeField);
        myPanel.add(new JLabel("Venue:"));
        venueField = new JTextField(10);
        myPanel.add(venueField);
        myPanel.add(new JLabel("Description:"));
        descriptionField = new JTextField(10);
        myPanel.add(descriptionField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter Event Details", JOptionPane.OK_CANCEL_OPTION);

        if (result == JOptionPane.OK_OPTION) {
            try {
                saveEventDetails();
                eventCreatedSuccessfully = true;

            }catch (Exception e){
                JOptionPane.showMessageDialog(null, "Error occurred while creating event: " + e.getMessage(),
                        "Error", JOptionPane.ERROR_MESSAGE);
                // Set eventCreatedSuccessfully to false
                eventCreatedSuccessfully = false;
            }
        } else {
            eventCreatedSuccessfully = false;
        }

        //Close the CreateEventView window and bring mainPanel to front



        if (eventCreatedSuccessfully) {
            JOptionPane.showMessageDialog(null, "Event created successfully!");
        }
    }

    public ArrayList<String> saveEventDetails() {
        ArrayList<String> eventDetails = new ArrayList<>();
        // Retrieve data from the fields
        String eventName = eventField.getText();
        String eventDate = dateField.getText();
        String eventTime = timeField.getText();
        String eventVenue = venueField.getText();
        String eventDescription = descriptionField.getText();
        eventDetails.add(eventName);
        eventDetails.add(eventDate);
        eventDetails.add(eventTime);
        eventDetails.add(eventVenue);
        eventDetails.add(eventDescription);
        return eventDetails;
    }

}


