package view;

import javax.swing.*;
import java.awt.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class OperateUserView extends JFrame {
    private JTextField emailField;
    private JTextField nameField;
    private JTextField passwordField;
    private String first;
    private String second;
    private String third;
    private int result;

    public OperateUserView(ArrayList<String> firstDetails, Socket socket) {
        try {
            first = firstDetails.get(0);
            second = firstDetails.get(1);
            third = firstDetails.get(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setTitle("edit User");
        setSize(300, 200);

        JPanel myPanel = new JPanel();
        myPanel.setLayout(new GridLayout(5, 2));
        myPanel.add(new JLabel("email: "));
        emailField = new JTextField(10);
        emailField.setText(first);
        myPanel.add(emailField);
        myPanel.add(new JLabel("username: "));
        nameField = new JTextField(10);
        nameField.setText(second);
        myPanel.add(nameField);
        myPanel.add(new JLabel("password: "));
        passwordField = new JTextField(10);
        passwordField.setText(third);
        myPanel.add(passwordField);
        result = JOptionPane.showConfirmDialog(null, myPanel,
                "edit user", JOptionPane.OK_CANCEL_OPTION);
        editUser(firstDetails, socket);
        dispose();
    }

    public void editUser(ArrayList<String> old, Socket socket) {
        ArrayList<String> userAdd = new ArrayList<>();
        if (result == JOptionPane.OK_OPTION) {
            userAdd.add(emailField.getText());
            userAdd.add(nameField.getText());
            userAdd.add(passwordField.getText());
            try {
                ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
                writer.writeUTF("editUser");
                writer.writeObject(old);
                writer.writeObject(userAdd);
                writer.flush();
                JOptionPane.showMessageDialog(null, "editing successful");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        } else{
            dispose();
        }
        dispose();
    }
}
