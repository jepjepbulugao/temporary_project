package view;

import javax.swing.*;
import java.awt.*;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class SearchEventView extends JFrame {
    private JTextField nameField;
    private JTextField dateField;
    private JTextField timeField;
    private JTextField venueField;
    private JTextField descField;
    private String first;
    private String second;
    private String third;
    private String fourth;
    private String fifth;
    private int result;

    public SearchEventView(ArrayList<String> firstDetails) {
        try {
            first = firstDetails.get(0);
            second = firstDetails.get(1);
            third = firstDetails.get(2);
            fourth = firstDetails.get(3);
            fifth = firstDetails.get(4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setTitle("edit Event");
        setSize(300, 200);

        JPanel myPanel = new JPanel();
        myPanel.setLayout(new GridLayout(5, 2));
        myPanel.add(new JLabel("name: "));
        nameField = new JTextField(10);
        nameField.setText(first);
        myPanel.add(nameField);
        myPanel.add(new JLabel("date: "));
        dateField = new JTextField(10);
        dateField.setText(second);
        myPanel.add(dateField);
        myPanel.add(new JLabel("time: "));
        timeField = new JTextField(10);
        timeField.setText(third);
        myPanel.add(timeField);
        myPanel.add(new JLabel("venue"));
        venueField = new JTextField(10);
        venueField.setText(fourth);
        myPanel.add(venueField);
        myPanel.add(new JLabel("description"));
        descField = new JTextField(10);
        descField.setText(fifth);
        myPanel.add(descField);
        result = JOptionPane.showConfirmDialog(null, myPanel,
                "Event Details", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            JOptionPane.showMessageDialog(null, "Event Shown",
                    "Event",JOptionPane.ERROR_MESSAGE);
            dispose();
        }
    }

}
