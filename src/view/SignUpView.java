package view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

public class SignUpView extends JFrame {
    private JTextField emailField;
    private JTextField firstNameField;
    private JTextField lastNameField;
    private JPasswordField passwordField;
    private JButton signUpButton;

    public SignUpView(Socket socket) {
        initializeUI();
        setupLayout();
        setupActions(socket);
    }

    private void initializeUI() {
        setTitle("Sign Up");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
        getContentPane().setBackground(Color.WHITE);
    }

    private void setupLayout() {
        JPanel mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBackground(Color.WHITE);
        mainPanel.setBorder(new EmptyBorder(20, 20, 20, 20)); // Add padding

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);

        JLabel createAccountLabel = new JLabel("Create an Account");
        createAccountLabel.setFont(new Font("Segoe UI", Font.BOLD, 24)); // Modern font
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 2;
        mainPanel.add(createAccountLabel, gbc);

        JLabel emailLabel = new JLabel("Email:");
        emailLabel.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        mainPanel.add(emailLabel, gbc);

        emailField = new JTextField(20);
        emailField.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        mainPanel.add(emailField, gbc);

        JLabel firstNameLabel = new JLabel("First Name:");
        firstNameLabel.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridx = 0;
        gbc.gridy = 2;
        mainPanel.add(firstNameLabel, gbc);

        firstNameField = new JTextField(20);
        firstNameField.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridx = 1;
        gbc.gridy = 2;
        mainPanel.add(firstNameField, gbc);

        JLabel lastNameLabel = new JLabel("Last Name:");
        lastNameLabel.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridx = 0;
        gbc.gridy = 3;
        mainPanel.add(lastNameLabel, gbc);

        lastNameField = new JTextField(20);
        lastNameField.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridx = 1;
        gbc.gridy = 3;
        mainPanel.add(lastNameField, gbc);

        JLabel passwordLabel = new JLabel("Password:");
        passwordLabel.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridx = 0;
        gbc.gridy = 4;
        mainPanel.add(passwordLabel, gbc);

        passwordField = new JPasswordField(20);
        passwordField.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridx = 1;
        gbc.gridy = 4;
        mainPanel.add(passwordField, gbc);

        signUpButton = new JButton("Sign Up");
        signUpButton.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        mainPanel.add(signUpButton, gbc);

        add(mainPanel);
        pack();
        setLocationRelativeTo(null); // Center the window
    }

    private void setupActions(Socket socket) {
        signUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String signUpAttempt = "signup";
                String email = emailField.getText();
                String firstName = firstNameField.getText();
                String lastName = lastNameField.getText();
                String fullName = firstName.concat(" "+lastName);
                char[] tempPassword = passwordField.getPassword();
                String password = new String(tempPassword);
                String user = "user";
                ArrayList<String> request = new ArrayList<>();
                request.add(user);
                request.add(email);
                request.add(fullName);
                request.add(password);
                try {
                    ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
                    writer.writeUTF(signUpAttempt);
                    writer.flush();
                    writer.writeObject(request);
                }catch (Exception e1){
                    e1.printStackTrace();
                }
                // Clear fields after signing up
                emailField.setText("");
                firstNameField.setText("");
                lastNameField.setText("");
                passwordField.setText("");

                // Dispose of the sign-up window
                dispose();

                // Open the login window
                LoginView loginView = new LoginView(socket);
                loginView.setVisible(true);
            }
        });
    }
}

