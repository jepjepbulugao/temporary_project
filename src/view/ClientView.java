package view;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class ClientView extends JFrame {
    private JList<String> userList;
    private DefaultListModel<String> userListModel;
    private JTextField searchField;
    private JButton searchButton;
    private JButton addButton;
    private JButton deleteButton;
    private JButton logoutButton;

    public ClientView(Socket socket) {
        setTitle("Manage Events");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setBackground(new Color(240, 240, 240));

        // Initialize components
        initializeComponents();

        // Set layout
        setLayout(new BorderLayout(20, 20));
        add(createSearchPanel(), BorderLayout.NORTH);
        add(createButtonPanel(), BorderLayout.WEST);
        add(createUserListPanel(), BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
        logOut(socket);
    }

    private void initializeComponents() {
        userListModel = new DefaultListModel<>();
        userList = new JList<>(userListModel);
        searchField = new JTextField();
        searchButton = new JButton("Search");
        addButton = new JButton("Add Event");
        logoutButton = new JButton("Delete Event");
        deleteButton = new JButton("LOGOUT");

        Font buttonFont = new Font("Segoe UI", Font.PLAIN, 20);
        addButton.setFont(buttonFont);
        deleteButton.setFont(buttonFont);
        logoutButton.setFont(buttonFont);

        searchButton.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        searchField.setFont(new Font("Segoe UI", Font.PLAIN, 20));
    }

    private JPanel createSearchPanel() {
        JPanel searchPanel = new JPanel(new BorderLayout());
        searchPanel.setBackground(new Color(240, 240, 240));
        searchPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        searchPanel.add(searchField, BorderLayout.CENTER);
        searchPanel.add(searchButton, BorderLayout.EAST);
        return searchPanel;
    }

    private JPanel createButtonPanel() {
        JPanel buttonPanel = new JPanel(new GridLayout(3, 1, 0, 20));
        buttonPanel.setBackground(new Color(240, 240, 240));
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        buttonPanel.add(addButton);
        buttonPanel.add(logoutButton);
        buttonPanel.add(deleteButton);
        return buttonPanel;
    }

    private JScrollPane createUserListPanel() {
        JPanel userListPanel = new JPanel(new BorderLayout());
        userListPanel.setBackground(new Color(240, 240, 240));
        userListPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        userListPanel.add(new JScrollPane(userList), BorderLayout.CENTER); // Adding JList to JScrollPane

        JPanel emptyPanel = new JPanel(); // Create an empty panel to center the JList horizontally
        emptyPanel.setBackground(new Color(240, 240, 240));
        userListPanel.add(emptyPanel, BorderLayout.EAST); // Add the empty panel to the right side

        return new JScrollPane(userListPanel);
    }

    public void operateUser(Socket socket){

        userList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3){
                    int index = userList.locationToIndex(e.getPoint());
                    if(index != -1){
                        System.out.println("item: " + userListModel.getElementAt(index));
                        String user = userListModel.getElementAt(index);
                        try {
                            ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
                            writer.writeUTF("sendUser");
                            writer.writeUTF(user);
                            writer.flush();
                            ObjectInputStream reader1 = new ObjectInputStream(socket.getInputStream());
                            ArrayList<String> details = (ArrayList<String>) reader1.readObject();
                            OperateUserView operateUserView = new OperateUserView(details,socket);
                            operateUserView.setVisible(true);
                        }catch (Exception e2){
                            e2.printStackTrace();
                        }

                    }
                }
            }
        });
    }
    public void loadUsersFromXML(Socket socket) {
        try {
            ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
            writer.writeUTF("sendEvent");
            writer.flush();
            ObjectInputStream adminReader = new ObjectInputStream(socket.getInputStream());
            ArrayList<String> usernames = (ArrayList<String>) adminReader.readObject();
            for (String username : usernames) {
                System.out.println(username);
                userListModel.addElement(username);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error loading users from XML file: " + e.getMessage());
        }
    }
    public void setUpDelete(Socket socket){
        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = userList.getSelectedIndex();
                String name = userListModel.getElementAt(index);
                System.out.println("delete User: " + name);
                int option = JOptionPane.showConfirmDialog(null, "Delete this Event?", "Delete User", JOptionPane.YES_NO_OPTION);
                if (option == JOptionPane.YES_OPTION) {
                    try {
                        ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
                        writer.writeUTF("deleteEvent");
                        writer.writeUTF(name);
                        writer.flush();
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                    userListModel.remove(index);
                }
            }
        });
    }

    public void setUpAdd(Socket socket){
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               CreateEventView createEventView = new CreateEventView();
                ArrayList<String> temp = createEventView.saveEventDetails();
                try {
                    ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
                    writer.writeUTF("addEvent");
                    writer.writeObject(temp);
                    writer.flush();
                }catch (Exception e2){
                    e2.printStackTrace();
                }
            }
        });
    }
    public void search(Socket socket){
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String searchItem = searchField.getText();
                for(int i = 0; i < userListModel.getSize();i++){
                    if(searchItem.equalsIgnoreCase(userListModel.getElementAt(i))){
                        try {
                            ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
                            writer.writeUTF("sendEventDetails");
                            writer.writeUTF(searchItem);
                            writer.flush();
                            ObjectInputStream reader1 = new ObjectInputStream(socket.getInputStream());
                            ArrayList<String> details = (ArrayList<String>) reader1.readObject();
                            SearchEventView searchEventView = new SearchEventView(details);
                            searchEventView.setVisible(true);
                        }catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                }
            }
        });
    }
    public void logOut(Socket socket) {
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        new LoginView(socket).setVisible(true);
                    }
                });
            }
        });
    }
}
