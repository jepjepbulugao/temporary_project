package view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

public class LoginView extends JFrame {
    private JPanel mainPanel;
    private JButton loginButton;
    private JButton signUpButton;
    private JRadioButton adminButton;
    private JPasswordField passwordField;
    private JTextField txtUserEmail;


    public LoginView(Socket socket) {
        initializeUI();
        setupLayout();
        setupActions(socket);
        sendLogin(socket);
    }

    private void initializeUI() {
        setTitle("Event Manager");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setBackground(new Color(240, 240, 240)); // Light gray background
    }

    private void setupLayout() {
        mainPanel = new JPanel(new BorderLayout());
        mainPanel.setBackground(new Color(240, 240, 240));
        mainPanel.setBorder(new EmptyBorder(20, 50, 20, 50)); // Add padding

        JLabel loginLabel = new JLabel("Login");
        loginLabel.setHorizontalAlignment(SwingConstants.CENTER);
        loginLabel.setFont(new Font("Segoe UI", Font.BOLD, 28)); // Modern font
        mainPanel.add(loginLabel, BorderLayout.NORTH);

        JPanel inputPanel = new JPanel(new GridBagLayout());
        inputPanel.setBackground(new Color(240, 240, 240));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);

        JLabel emailLabel = new JLabel("Email:");
        emailLabel.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        inputPanel.add(emailLabel, gbc);

        txtUserEmail = new JTextField(20);
        txtUserEmail.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        inputPanel.add(txtUserEmail, gbc);

        JLabel passwordLabel = new JLabel("Password:");
        passwordLabel.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridy = 2;
        inputPanel.add(passwordLabel, gbc);

        passwordField = new JPasswordField(20);
        passwordField.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        gbc.gridy = 3;
        inputPanel.add(passwordField, gbc);

        // Add vertical spacing between login label and input fields
        gbc.gridy = 4;
        gbc.weighty = 1;
        inputPanel.add(Box.createVerticalStrut(20), gbc);

        loginButton = new JButton("Login");
        loginButton.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        signUpButton = new JButton("Sign Up");
        signUpButton.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font
        adminButton = new JRadioButton("Admin");
        adminButton.setFont(new Font("Segoe UI", Font.PLAIN, 16)); // Modern font

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 10));
        buttonPanel.setBackground(new Color(240, 240, 240));
        buttonPanel.add(loginButton);
        buttonPanel.add(signUpButton);
        buttonPanel.add(adminButton);

        mainPanel.add(inputPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        add(mainPanel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(null);
    }

    private void setupActions(Socket socket) {
        signUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openSignUpWindow(socket);
            }
        });
    }

    private void openSignUpWindow(Socket socket) {
        SignUpView signUpView = new SignUpView(socket);
        signUpView.setVisible(true);
        dispose();
    }

    private void sendLogin(Socket socket) {
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ArrayList<String> login = new ArrayList<>();
                String email = txtUserEmail.getText();
                char[] passwordchar = passwordField.getPassword();
                String password = new String(passwordchar);
                String isAdmin = String.valueOf(adminButton.isSelected());
                String loginAttempt = "login";
                login.add(isAdmin);
                login.add(email);
                login.add(password);
                try {
                    ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
                    writer.writeUTF(loginAttempt);
                    writer.writeObject(login);
                } catch (IOException ee) {
                    throw new RuntimeException(ee);
                }
                dispose();
            }
        });
    }
    public String getUserEmail() {
        return txtUserEmail.getText();
    }

    public char[] getPassword() {
        return passwordField.getPassword();
    }
}
