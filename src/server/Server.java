package server;
import java.io.*;

import model.AdminModel;
import model.ClientModel;
import model.LoginModel;
import model.SignUpModel;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Server {
    private static final int PORT = 8081;
    private static final int MAX_CLIENTS = 5;
    private static Executor executor = Executors.newFixedThreadPool(MAX_CLIENTS);

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            String canonicalName = InetAddress.getLocalHost().getCanonicalHostName();
            System.out.println("Server started. Listening on port "+ PORT+" on " +canonicalName);

            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client connected: " + clientSocket.getInetAddress());

                executor.execute(new ClientHandler(clientSocket));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class ClientHandler implements Runnable {
        private final Socket clientSocket;

        public ClientHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }
        @Override
        public void run() {

                try {
                    do {
                    ObjectInputStream reader = new ObjectInputStream(clientSocket.getInputStream());
                    ObjectOutputStream writer = new ObjectOutputStream(clientSocket.getOutputStream());
                    String request = reader.readUTF();
                    System.out.println(request);
                    switch (request) {
                        case "login":
                            try {
                                ArrayList<String> loginDetails = (ArrayList<String>) reader.readObject();
                                String first = loginDetails.get(0);
                                String second = loginDetails.get(1);
                                String third = loginDetails.get(2);
                                for(String detail:loginDetails){
                                    System.out.println(detail);
                                }
                                String response = LoginModel.login(first, second, third);
                                System.out.println(response);
                                writer.writeUTF(response);
                                writer.flush();
                            } catch (Exception loginError) {
                                loginError.printStackTrace();
                            }
                            break;
                        case "signup":
                            try {
                                ArrayList<String> signUpDetails = (ArrayList<String>) reader.readObject();
                                String first = signUpDetails.get(0);
                                String second = signUpDetails.get(1);
                                String third = signUpDetails.get(2);
                                String fourth = signUpDetails.get(3);
                                for(String detail: signUpDetails){
                                    System.out.println(detail);
                                }
                                SignUpModel.addUser(first,third,second,fourth);
                                String response = "signup";
                                writer.writeUTF(response);
                                writer.flush();
                            } catch (Exception signUpError) {
                                signUpError.printStackTrace();
                            }
                            break;
                        case "readAccounts":
                            try{
                                ArrayList<String> sendUsers= AdminModel.sendAccounts();
                                for(String s:sendUsers){
                                    System.out.println(s);
                                }
                                writer.writeObject(sendUsers);
                                writer.flush();
                            }catch (Exception readAccException){
                                readAccException.printStackTrace();
                            }
                            break;
                        case "addUser":
                            try{
                                ArrayList<String> newUser = (ArrayList<String>) reader.readObject();
                                String zero = newUser.get(0);
                                String first = newUser.get(1);
                                String second = newUser.get(2);
                                String third = newUser.get(3);
                                SignUpModel.addAdmin(third,first,zero,second);
                            }catch (Exception addUserException){
                                addUserException.printStackTrace();
                            }
                            break;
                        case "sendUser":
                            try {
                                String user = reader.readUTF();
                                ArrayList<String> details = AdminModel.sendUserDetails(user);
                                writer.writeObject(details);
                                writer.flush();
                            }catch (Exception sendUserException){
                                sendUserException.printStackTrace();
                            }
                            break;
                        case "editUser":
                            try {
                                ArrayList<String> oldDetails = (ArrayList<String>) reader.readObject();
                                ArrayList<String> newDetails = (ArrayList<String>) reader.readObject();
                                AdminModel.editUser(oldDetails,newDetails);
                                writer.writeUTF("editSuccess");
                            }catch (Exception editUserException){
                                editUserException.printStackTrace();
                            }
                            break;
                        case "deleteUser":
                            try{
                                String user = reader.readUTF();
                                System.out.println(user);
                                AdminModel.deleteUser(user);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        case "sendEvent":
                            try {
                                ArrayList<String> events = ClientModel.sendEvents();
                                writer.writeObject(events);
                                writer.flush();
                            }catch (Exception e1){
                                e1.printStackTrace();
                            }
                            break;
                        case "addEvent":
                            try{
                                ArrayList<String> newEvent = (ArrayList<String>) reader.readObject();
                                String name = newEvent.get(0);
                                String date = newEvent.get(1);
                                String time = newEvent.get(2);
                                String venue = newEvent.get(3);
                                String desc = newEvent.get(4);
                                ClientModel.addEvent(name,date,time,venue,desc);
                            }catch (Exception addEventEx){
                                addEventEx.printStackTrace();
                            }
                        case "deleteEvent":
                            try{
                                String name = reader.readUTF();
                                System.out.println(name);
                                ClientModel.deleteEvent(name);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        case "sendEventDetails":
                            try {
                                String name = reader.readUTF();
                                ArrayList<String> details = ClientModel.sendEventDetails(name);
                                writer.writeObject(details);
                                writer.flush();
                            }catch (Exception sendEDExc){
                                sendEDExc.printStackTrace();
                            }
                            break;
                    }
            }while (clientSocket.isConnected());
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }
}
