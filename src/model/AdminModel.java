package model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;

public class AdminModel {
    public AdminModel() {
    }

    public static ArrayList<String> sendAccounts() {
        ArrayList<String> sendAccounts = new ArrayList<>();
        try {
            File xmlFile = new File("res/accounts.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList accountList = doc.getElementsByTagName("account");
            for (int i = 0; i < accountList.getLength(); i++) {
                Node account = accountList.item(i);
                if (account.getNodeType() == Node.ELEMENT_NODE) {
                    Element accountElement = (Element) account;
                    NodeList usernameList = accountElement.getElementsByTagName("username");
                    if (usernameList.getLength() > 0) {
                        String username = usernameList.item(0).getTextContent();
                        sendAccounts.add(username);
                    }
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return sendAccounts;
    }


    public static ArrayList<String> sendUserDetails(String user) {
        ArrayList<String> details = new ArrayList<>();
        try {
            File inputFile = new File("res/accounts.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nodes = doc.getElementsByTagName("account");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                Element element = (Element) node;
                if (user.equalsIgnoreCase(element.getElementsByTagName("username").item(0).getTextContent())) {
                    details.add(element.getElementsByTagName("email").item(0).getTextContent());
                    details.add(user);
                    details.add(element.getElementsByTagName("password").item(0).getTextContent());
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return details;
    }
    public static void editUser(ArrayList<String> details,ArrayList<String> newDetails){
       try{
        File accountsFile = new File("res/accounts.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(accountsFile);
        document.getDocumentElement().normalize();
        NodeList nodes = document.getElementsByTagName("account");
        for (int i = 0; i < nodes.getLength(); i++){
            Node node = nodes.item(i);
            Element element = (Element) node;
            if(details.get(0).equalsIgnoreCase(element.getElementsByTagName("email").item(0).getTextContent())){
                 element.getElementsByTagName("email").item(0).setTextContent(newDetails.get(0));
                 element.getElementsByTagName("username").item(0).setTextContent(newDetails.get(1));
                 element.getElementsByTagName("password").item(0).setTextContent(newDetails.get(2));
            }
        }
           TransformerFactory transformerFactory = TransformerFactory.newInstance();
           Transformer transformer = transformerFactory.newTransformer();
           DOMSource source = new DOMSource(document);
           StreamResult result = new StreamResult(new File("res/accounts.xml"));
           transformer.transform(source, result);
    }catch (Exception editUserError){
        editUserError.printStackTrace();
    }
    }
    public static void deleteUser(String user) {
        try {
            File inputFile = new File("res/accounts.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(inputFile);
            document.getDocumentElement().normalize();

            NodeList accountList = document.getElementsByTagName("account");
            boolean userDeleted = false;
            for (int i = 0; i < accountList.getLength(); i++) {
                Node userNode = accountList.item(i);
                Element element = (Element) userNode;
                if (element.getElementsByTagName("username").item(0).getTextContent().equalsIgnoreCase(user)) {
                    Node parent = userNode.getParentNode();
                    parent.removeChild(userNode);
                    userDeleted = true;
                    break; // Once user is deleted, no need to continue searching
                }
            }

            if (userDeleted) {
                // Write the updated Document object back to the XML file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(document);
                StreamResult result = new StreamResult(new File("res/accounts.xml"));
                transformer.transform(source, result);

                System.out.println("User with username " + user + " deleted successfully.");
            } else {
                System.out.println("User with username " + user + " not found.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
