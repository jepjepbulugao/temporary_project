package model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LoginModel {
    private Document document;

    public LoginModel(Document document) {
        this.document = document;
    }

    public static String login(String admin, String email, String password) {
        String success = "";
        try {
            File accounts = new File("res/accounts.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(accounts);
            document.getDocumentElement().normalize();

            if (admin.equalsIgnoreCase("true")) {
                NodeList admins = document.getElementsByTagName("admin");
                success = matchAdmin(admins, email, password);
            } else {
                NodeList users = document.getElementsByTagName("user");
                success = matchUser(users, email, password);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    public static String matchAdmin(NodeList nodes, String email, String password) {
        String response = "";
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element element = (Element) node;
            String emailCompared = element.getElementsByTagName("email").item(0).getTextContent();
            String passwordCompared = element.getElementsByTagName("password").item(0).getTextContent();
            if (email.equalsIgnoreCase(emailCompared) && password.equalsIgnoreCase(passwordCompared)) {
                response = "admin";
                break;
            }
        }
        if (response.isEmpty()) {
            response = "again";

        }
        return response;
        }


    public static String matchUser(NodeList nodes, String email, String password) {
        String response = "";
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element element = (Element) node;
            String emailCompared = element.getElementsByTagName("email").item(0).getTextContent();
            String passwordCompared = element.getElementsByTagName("password").item(0).getTextContent();
            if (email.equalsIgnoreCase(emailCompared) && password.equalsIgnoreCase(passwordCompared)) {
                response = "user";
                break;
            }
        }
        if (response.isEmpty()) {
            response = "again";
        }
        return response;
    }
}
