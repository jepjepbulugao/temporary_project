package model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.print.Doc;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class SignUpModel {

    public SignUpModel(Document document){
    }
    public static void addUser(String type,String fullName, String email, String password){
        try {
            File accountsFile = new File("res/accounts.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(accountsFile);

            Element accounts = document.getDocumentElement();

            Element account = document.createElement("account");
            Element user = document.createElement("user");
            Element userEmail = document.createElement("email");
            Element userName = document.createElement("username");
            Element userPassword = document.createElement("password");

            userName.setTextContent(fullName);
            userPassword.setTextContent(password);
            userEmail.setTextContent(email);

            user.appendChild(userEmail);
            user.appendChild(userName);
            user.appendChild(userPassword);

            account.appendChild(user);
            accounts.appendChild(account);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File("res/accounts.xml"));
            transformer.transform(source, result);

        }catch (Exception adduserError){
            adduserError.printStackTrace();
        }
    }
    public static void addAdmin(String type,String fullName, String email, String password){
        try {
            File accountsFile = new File("res/accounts.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(accountsFile);

            Element accounts = document.getDocumentElement();

            Element account = document.createElement("account");
            Element user = document.createElement(type);
            Element userEmail = document.createElement("email");
            Element userName = document.createElement("username");
            Element userPassword = document.createElement("password");

            userName.setTextContent(fullName);
            userPassword.setTextContent(password);
            userEmail.setTextContent(email);

            user.appendChild(userEmail);
            user.appendChild(userName);
            user.appendChild(userPassword);

            account.appendChild(user);
            accounts.appendChild(account);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File("res/accounts.xml"));
            transformer.transform(source, result);

        }catch (Exception adduserError){
            adduserError.printStackTrace();
        }
    }
}
