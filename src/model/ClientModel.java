package model;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;

public class ClientModel {
    public ClientModel() {
    }

    public static boolean findEvent(String searchTerm) {
        boolean temp = false;
        try {
            File xmlFile = new File("res/events.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();

            // Find elements with the given search term
            NodeList nodeList = doc.getElementsByTagName("*");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eventElement = (Element) node;
                    NodeList eventList = eventElement.getChildNodes();
                    for (int j = 0; j < eventList.getLength(); j++) {
                        Node childNode = eventList.item(j);
                        if (childNode.getNodeType() == Node.TEXT_NODE) {
                            String text = childNode.getTextContent().trim();
                            temp = (text.equals(searchTerm));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return temp;
    }

    public static ArrayList<String> sendEventDetails(String name) {
        ArrayList<String> details = new ArrayList<>();
        try {
            File inputFile = new File("res/events.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nodes = doc.getElementsByTagName("event");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                Element element = (Element) node;
                if (name.equalsIgnoreCase(element.getElementsByTagName("name").item(0).getTextContent())) {
                    details.add(name);
                    details.add(element.getElementsByTagName("date").item(0).getTextContent());
                    details.add(element.getElementsByTagName("time").item(0).getTextContent());
                    details.add(element.getElementsByTagName("venue").item(0).getTextContent());
                    details.add(element.getElementsByTagName("description").item(0).getTextContent());
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return details;
    }
    public static ArrayList<String> sendEvents() {
        ArrayList<String> sendAccounts = new ArrayList<>();
        try {
            File xmlFile = new File("res/events.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList accountList = doc.getElementsByTagName("event");
            for (int i = 0; i < accountList.getLength(); i++) {
                Node account = accountList.item(i);
                if (account.getNodeType() == Node.ELEMENT_NODE) {
                    Element accountElement = (Element) account;
                    NodeList usernameList = accountElement.getElementsByTagName("name");
                    if (usernameList.getLength() > 0) {
                        String username = usernameList.item(0).getTextContent();
                        sendAccounts.add(username);
                    }
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return sendAccounts;
    }
    /*public static ArrayList<String> sendEventDetails(String name) {
        ArrayList<String> details = new ArrayList<>();
        try {
            File inputFile = new File("res/events.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nodes = doc.getElementsByTagName("event");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                Element element = (Element) node;
                if (name.equalsIgnoreCase(element.getElementsByTagName("name").item(0).getTextContent())) {
                    details.add(name);
                    details.add(element.getElementsByTagName("date").item(0).getTextContent());
                    details.add(element.getElementsByTagName("time").item(0).getTextContent());
                    details.add(element.getElementsByTagName("venue").item(0).getTextContent());
                    details.add(element.getElementsByTagName("description").item(0).getTextContent());
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return details;
    }*/

    public static void deleteEvent(String name) {
        try {
            File inputFile = new File("res/events.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(inputFile);
            document.getDocumentElement().normalize();

            NodeList accountList = document.getElementsByTagName("event");
            boolean userDeleted = false;
            for (int i = 0; i < accountList.getLength(); i++) {
                Node userNode = accountList.item(i);
                Element element = (Element) userNode;
                if (element.getElementsByTagName("name").item(0).getTextContent().equalsIgnoreCase(name)) {
                    Node parent = userNode.getParentNode();
                    parent.removeChild(userNode);
                    userDeleted = true;
                    break; // Once user is deleted, no need to continue searching
                }
            }

            if (userDeleted) {
                // Write the updated Document object back to the XML file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(document);
                StreamResult result = new StreamResult(new File("res/events.xml"));
                transformer.transform(source, result);

                System.out.println("User with username " + name + " deleted successfully.");
            } else {
                System.out.println("User with username " + name + " not found.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void addEvent(String name1, String date1, String time1, String venue1, String desc1){
        try{
            File file = new File("res/events.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();
            Element events = document.getDocumentElement();
            Element event = document.createElement("event");
            Element name = document.createElement("name");
            Element date = document.createElement("date");
            Element time = document.createElement("time");
            Element venue = document.createElement("venue");
            Element desc = document.createElement("description");
            name.setTextContent(name1);
            date.setTextContent(date1);
            time.setTextContent(time1);
            venue.setTextContent(venue1);
            desc.setTextContent(desc1);
            event.appendChild(name);
            event.appendChild(date);
            event.appendChild(time);
            event.appendChild(venue);
            event.appendChild(desc);
            events.appendChild(event);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File("res/events.xml"));
            transformer.transform(source, result);
        }catch (Exception e1){
            e1.printStackTrace();
        }
    }
}